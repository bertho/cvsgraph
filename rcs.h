/*
 * CvsGraph graphical representation generator of brances and revisions
 * of a file in cvs/rcs.
 *
 * Copyright (C) 2001  B. Stultiens
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RCS_H
#define __RCS_H

struct __revision_t;	/* Forward */

typedef struct __rev_t
{
	char	*branch;
	char	*rev;
	int	isbranch;
} rev_t;

typedef struct __revs_t
{
	rev_t	**revs;
	int	nrevs;
} revs_t;

typedef struct __tag_t
{
	char			*tag;
	rev_t			*rev;
	struct __revision_t	*logrev;	/* backpointer to logical revision */
	int			yofs;		/* Internal drawing offset to center of the tag */
	int			ignore;		/* This tag is marked ignored for drawing */
} tag_t;

typedef struct __tags_t
{
	tag_t	**tags;
	int	ntags;
} tags_t;

typedef union __tag_revision_t
{
	tag_t			*tag;
	struct __revision_t	*rev;
} tag_revision_t;

typedef enum __tag_revision_e
{
	TR_INVALID = 0,
	TR_TAG,
	TR_REVISION
} tag_revision_e;

typedef struct __merge_t
{
	tag_revision_e	type;
	tag_revision_t	to;		/* This tag/revision is destination of a merge */
	tag_revision_t	from;		/* This tag/revision is origin of a merge */
	int		clr;		/* Color reference for multiple regex match statements */
} merge_t;

typedef struct __idrev_t
{
	char	*id;
	rev_t	*rev;
} idrev_t;

typedef struct __idrevs_t
{
	idrev_t	**idrevs;
	int	nidrevs;
} idrevs_t;

typedef struct __ids_t
{
	char	**ids;
	int	nids;
} ids_t;

typedef struct __dtext_t
{
	rev_t	*rev;			/* Diff/log revision */
	char	*log;			/* The log entry */
	char	*text;			/* Diff text (only set if lexer send it) */
} dtext_t;

typedef struct __dtexts_t
{
	dtext_t	**dtexts;
	int	ndtexts;
} dtexts_t;

typedef enum __phrase_type_e	/* Extra phrases in deltas */
{
	PT_INVALID = 0,
	PT_MERGEPOINT		/* CVSNT: mergepoint1 <revision-string> */
} phrase_type_e;

typedef struct __phrase_t
{
	phrase_type_e	type;
	rev_t		*rev;
} phrase_t;

typedef struct __phrases_t
{
	phrase_t	**phrases;
	int		nphrases;
} phrases_t;

typedef struct __delta_t
{
	rev_t		*rev;		/* Delta revision */
	char		*date;
	char		*author;
	char		*state;
	revs_t		*branches;
	rev_t		*next;		/* Next delta in this branch */
	phrases_t	*phrases;	/* Extra phrases in the delta */
	phrase_t	*mergepoint;	/* CVSNT: mergepoint1 */
	int		flag;		/* Set if assigned a branch to prevent infinite recursion */
} delta_t;

typedef struct __deltas_t
{
	delta_t	**deltas;
	int	ndeltas;
} deltas_t;

typedef struct __branch_t	/* Logical branch structure */
{
	struct __revision_t	**revs;
	int			nrevs;
	struct __revision_t	*branchpoint;	/* Backlink to revision that spawned the branch */
	rev_t			*branch;	/* Branch id */
	tag_t			**tags;		/* Symbolic tags */
	int			ntags;
	int			subtree_draw;	/* Set if subtree drawing should draw this branch */
	int			folded;		/* Set if this branch is folded in another box */
	struct __branch_t	*folded_to;	/* Set to the branch where this one is folded into */
	struct __branch_t	**folds;	/* List of other branches folded into this one */
	int			nfolds;
	int			fw;		/* Max width of the branch number if folds present */
	int			w, h;		/* BBox width/height */
	int			cx, y;		/* BBox center-top position */
	int			tw, th;		/* Total BBox width/height */
} branch_t;

typedef struct __revision_t	/* Logical revision structure */
{
	delta_t		*delta;
	dtext_t		*dtext;
	rev_t		*rev;		/* Shortcut to delta->rev */
	char		*revtext;	/* Expanded text to draw */
	char		*revidtext;	/* Expanded ID text to draw (normally revision number) */
	branch_t	*branch;	/* The branch this revision belongs to */
	branch_t	**branches;	/* Branches from this revision */
	int		nbranches;
	tag_t		**tags;		/* Symbolic tags */
	int		ntags;
	int		w, h;		/* BBox width/height */
	int		cx, y;		/* BBox center-top position */
	int		stripped;	/* Set if we jumped revisions due to strip_untagged */
	int		mergetarget;	/* Set if a cvsnt mergepoint starts/ends here */
} revision_t;

typedef struct __rcsfile_t
{
	char		*root;		/* The CVS root directory */
	char		*module;	/* The CVS module */
	char		*file;		/* The CVS filename */
	rev_t		*head;		/* Head revision */
	rev_t		*branch;	/* Active branch (NULL if main trunk) */
	ids_t		*access;	/* Access identifier(s) (NULL if none) */
	tags_t		*tags;		/* Symbolic tags (NULL if none) */
	idrevs_t	*locks;		/* Locks revisions (NULL if no locks) */
	int		strict;		/* Locking strategy */
	char		*comment;	/* File comment (NULL if none) */
	char		*expand;	/* Keyword substitution (NULL if none, i.e. -kv) */
	deltas_t	*deltas;	/* Delta admin */
	char		*desc;		/* Eh... description of file? */
	dtexts_t	*dtexts;	/* Diffs and logs */

	/* Logical structure */
	delta_t		**sdelta;	/* Sorted delta on revision */
	int		nsdelta;
	dtext_t		**sdtext;	/* Sorted dtext on revision */
	int		nsdtext;
	revision_t	**srev;		/* Sorted list of all revisions */
	int		nsrev;
	branch_t	**branches;	/* List of branches */
	branch_t	*active;	/* The active branch (translated branch of admin) */
	int		nbranches;
	int		nfolds;		/* Account folds to keep total branch-count */
	merge_t		*merges;	/* Merge tags from->to description */
	int		nmerges;
	int		tw, th;		/* Total BBox width/height */
} rcsfile_t;

void set_id(void);	/* Next scan is for an id */
void set_author(void);	/* Next scan is for an author */
void set_sym(void);	/* Next scan is for a symbol */
void set_skip(void);	/* Everyting until ';' dropped */
void set_skipstr(void);	/* Next string must be dropped */

int yylex(void);
int yyparse(void);

extern FILE *yyin;
extern rcsfile_t *rcsfile;

#endif
