/*
 * CvsGraph graphical representation generator of brances and revisions
 * of a file in cvs/rcs.
 *
 * Copyright (C) 2001  B. Stultiens
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __CVSGRAPH_READCONF_H
#define __CVSGRAPH_READCONF_H

/* Possible image type outputs. Gif is only supported on old versions of gd */
#define IMAGE_GIF	0
#define IMAGE_PNG	1
#define IMAGE_JPEG	2

extern int line_number;
void stack_option(const char *opt);
void read_config(const char *path);
color_t *get_colorref(const char *confcolor, int idx);

enum {
	TYPE_dummy = 256,
	TYPE_KEYWORD,
	TYPE_NUMBER,
	TYPE_VALUE,
	TYPE_BOOLEAN,
	TYPE_COLOR,
	TYPE_FONT,
	TYPE_STRING,
	TYPE_CSTRING,
	TYPE_DOUBLE,
	TYPE_COLORLIST,
	TYPE_STRINGLIST,
	OP_FIRST,
	OP_CONTAINED,
	OP_CONTAINEDI,
	OP_NCONTAINED,
	OP_NCONTAINEDI,
	OP_EQ,
	OP_NE,
	OP_GE,
	OP_GT,
	OP_LE,
	OP_LT,
	OP_LAST,
	KEY_STATE,
	KEY_AUTHOR,
	KEY_TAG,
	KEY_DATE,
	KEY_REV,
	KEY_UNKNOWN
};

#endif
