/*
 * CvsGraph graphical representation generator of brances and revisions
 * of a file in cvs/rcs.
 *
 * Copyright (C) 2001  B. Stultiens
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __CVSGRAPH_H
#define __CVSGRAPH_H

#define CONFFILENAME	"cvsgraph.conf"

#ifndef ETCDIR
# define ETCDIR		"/usr/local/etc"
#endif

#define DEBUG_CONF_LEX		0x01
#define DEBUG_CONF_YACC		0x02
#define DEBUG_RCS_LEX		0x04
#define DEBUG_RCS_YACC		0x08
#define DEBUG_RCS_FILE		0x10

#define HTMLLEVEL_3		1	/* HTML 3.x compatibility */
#define HTMLLEVEL_4		2	/* HTML 4.x compatibility */
#define HTMLLEVEL_X		3	/* XHTML compatibility */

extern int debuglevel;

typedef struct __msg_stack_t
{
	int	severity;
	int	w;
	int	h;
	char	*msg;
} msg_stack_t;

#define MSG_WARN	0
#define MSG_ERR		1

void stack_msg(int severity, const char *fmt, ...);


typedef struct __font_t
{
	gdFontPtr	gdfont;
	char		*ttfont;
	double		ttsize;
} font_t;

struct __node_t;

typedef struct __color_t
{
	int	r;
	int	g;
	int	b;
	int	id;
	struct __node_t	*node;
} color_t;

typedef struct __colorlist_t
{
	int	n;
	color_t	*clrs;
} colorlist_t;

typedef struct __node_t
{
	int		key;
	int		op;
	char		*content;
	struct __node_t	*tcase;
	struct __node_t	*fcase;
	union {
		char	*str;
		color_t	clr;
	} value;
} node_t;

typedef struct __stringlist_t
{
	int	n;
	char	**strs;
} stringlist_t;

typedef struct __condstring_t
{
	char	*str;
	node_t	*node;
} condstring_t;

typedef struct __config_t
{
	char	*cvsroot;
	char	*cvsmodule;
	char	*date_format;

	color_t	color_bg;
	int	transparent_bg;
	int	box_shadow;
	int	upside_down;
	int	left_right;
	int	strip_untagged;
	int	strip_first_rev;
	int	auto_stretch;
	int	use_ttf;
	int	anti_alias;
	int	thick_lines;
	int	parse_logs;
	int	html_level;

	font_t	msg_font;
	color_t	msg_color;

	font_t	tag_font;
	color_t	tag_color;
	char	*tag_ignore;
	int	tag_ignore_merge;
	int	tag_nocase;
	int	tag_negate;

	int	rev_hidenumber;
	font_t	rev_font;
	color_t	rev_color;
	color_t	rev_bgcolor;
	int	rev_separator;
	int	rev_minline;
	int	rev_maxline;
	int	rev_lspace;
	int	rev_rspace;
	int	rev_tspace;
	int	rev_bspace;
	condstring_t	rev_idtext;
	condstring_t	rev_text;
	color_t	rev_text_color;
	font_t	rev_text_font;
	int	rev_maxtags;

	colorlist_t	merge_color;
	stringlist_t	merge_from;
	stringlist_t	merge_to;
	int	merge_findall;
	int	merge_front;
	int	merge_nocase;
	int	merge_arrows;
	int	merge_cvsnt;
	color_t	merge_cvsnt_color;
	int	merge_on_tag;
	int	arrow_width;
	int	arrow_length;

	font_t	branch_font;
	color_t	branch_color;
	font_t	branch_tag_font;
	color_t	branch_tag_color;
	color_t	branch_bgcolor;
	int	branch_lspace;
	int	branch_rspace;
	int	branch_tspace;
	int	branch_bspace;
	int	branch_connect;
	int	branch_margin;
	int	branch_dupbox;
	int	branch_fold;
	int	branch_foldall;
	int	branch_resort;
	char	*branch_subtree;

	char	*title;
	int	title_x;
	int	title_y;
	font_t	title_font;
	int	title_align;
	color_t	title_color;

	int	margin_top;
	int	margin_bottom;
	int	margin_left;
	int	margin_right;

	int	image_type;
	int	image_quality;
	int	image_compress;
	int	image_interlace;

	char	*map_name;
	char	*map_branch_href;
	char	*map_branch_alt;
	char	*map_rev_href;
	char	*map_rev_alt;
	char	*map_diff_href;
	char	*map_diff_alt;
	char	*map_merge_href;
	char	*map_merge_alt;

	char	*expand[10];
} config_t;

extern config_t conf;

#endif
