#!/bin/sh

trace=.cvs_tag_tracer

# avoid recursion
if [ "$recursive_flag" != "stop" ]; then
  recursive_flag=stop
  if [ -f $trace ]; then
    branch=`cvs -n status $trace |grep branch:| awk '{print $4 $5}'`
    prefix=`cvs -n status $trace |grep branch:| awk '{print $3}'`
    merged=`fgrep branch: $trace`
    if [ "$branch" != "$merged" ]; then
      if [ `echo $merged | grep branch: | wc -l` -gt 0 ]; then
        lockdir=`cat CVS/Root`/`cat CVS/Repository`/\#cvs.lock
        (while [ -d $lockdir ]; do sleep 1; done; \
	cvs tag B_${prefix}_`fgrep merged_ $trace` $trace \
	2> /dev/null 1> /dev/null )&
      fi
    fi
  fi
fi
