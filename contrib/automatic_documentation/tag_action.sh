#!/bin/sh

trace=.cvs_tag_tracer
branch=`cvs status $trace | grep branch: | awk '{print $4 $5}'`

cvs update $trace 2> /dev/null 1> /dev/null
echo "This file is used for CVS administration. Please don't touch!" > $trace
echo merged_$1 >> $trace
echo $branch >> $trace
cvs add $trace 2> /dev/null 1> /dev/null
cvs commit -m "New tag" $trace 2> /dev/null 1> /dev/null
rm .\#$trace.* 2> /dev/null 1> /dev/null
true
