/*
 * CvsGraph graphical representation generator of brances and revisions
 * of a file in cvs/rcs.
 *
 * Copyright (C) 2001  B. Stultiens
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <gd.h>

#include "utils.h"
#include "cvsgraph.h"
#include "readconf.h"

const char *input_file;
int quiet;		/* Set if warnings should be suppressed */

void *xmalloc(size_t s)
{
	void *p = malloc(s);
	if(!p)
	{
		printf("Out of memory");
		exit(1);
	}
	memset(p, 0, s);
	return p;
}

void *xrealloc(void *a, size_t s)
{
	void *p;
	if(!a)
		return malloc(s);
	p = realloc(a, s);
	if(!p)
	{
		printf("Out of memory");
		exit(1);
	}
	return p;
}

char *xstrdup(const char *s)
{
	char *c;
	if(!s)
		s = "";
	c = xmalloc(strlen(s)+1);
	strcpy(c, s);
	return c;
}

void xfree(void *p)
{
	if(p)
		free(p);
}

static int do_msg(const char *type, const char *fmt, va_list va)
{
	fprintf(stderr, "%s: ", input_file);
	if(line_number)
		fprintf(stderr, "%d: ", line_number);
	if(type)
		fprintf(stderr, "%s: ", type);
	vfprintf(stderr, fmt, va);
	fprintf(stderr, "\n");
	return 0;
}

int yyerror(const char *fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	do_msg(NULL, fmt, va);
	va_end(va);
	exit(2);
	return 0;
}

int yywarning(const char *fmt, ...)
{
	va_list va;
	if(!quiet)
	{
		va_start(va, fmt);
		do_msg("warning", fmt, va);
		va_end(va);
	}
	return 0;
}

int rcserror(const char *fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	do_msg(NULL, fmt, va);
	va_end(va);
	exit(2);
	return 0;
}

