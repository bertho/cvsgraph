/*
 * CvsGraph graphical representation generator of brances and revisions
 * of a file in cvs/rcs.
 *
 * Copyright (C) 2001-2019  B. Stultiens
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

%x xSTR
%x xID
%x xSYM
%x xSKIP
%x xSKIPSTR
%x xAUTHOR

%option nounput
%option noinput

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <gd.h>

#include "utils.h"
#include "cvsgraph.h"
#include "readconf.h"
#include "rcs.h"
#include "rcsy.h"

#define SKIP_DELTATEXT	1

static void reset_str(void);
static void add_str(const char *s, int l);
static char *get_str(void);

static int skip_string = 0;

#define YY_NO_UNPUT 1

%}

ws		[\b\t\v\f\r ]
num		[0-9.]+
digit		[0-9]
idchar		[!-#%-+\-/<-?A-~\240-\377]
special		[$,.:;@]

%%
	/* RCS keywords */
head			return tHEAD;
branch			return tBRANCH;
access			return tACCESS;
symbols			return tSYMBOLS;
locks			return tLOCKS;
strict			return tSTRICT;
comment			return tCOMMENT;
expand			return tEXPAND;
date			return tDATE;
author			return tAUTHOR;
state			return tSTATE;
branches		return tBRANCHES;
next			return tNEXT;
desc			return tDESC;
log			return tLOG;
text			return tTEXT;
	/* CVS extensions */
owner			return tOWNER;
group			return tGROUP;
permissions		return tPERMISSIONS;
special			return tSPECIAL;
symlnk			return tSYMLINK;
hardlinks		return tHARDLINKS;
	/* Other known extensions */
namespace		return tNAMESPACE;
dead			return tDEAD;
	/* CVSNT extensions */
mergepoint1		return tMERGEPOINT;
deltatype		return tDELTATYPE;
commitid		return tCOMMITID;
kopt			return tKOPT;
filename		return tFILENAME;
properties		return tPROPERTIES;

	/* Here come any other 'newphrase' constructs */
{num}?{idchar}({idchar}|{num})*		{
		yylval.str = xstrdup(yytext);
		return tNEWPHRASE;
	}

	/* Special rules for skipping everything after a 'newphrase' part */
<xSKIP>[^;@\n]+		;
<xSKIP>\n		line_number++;
<xSKIP>@		BEGIN(xSKIPSTR);
<xSKIP>;		BEGIN(INITIAL); return *yytext;

<xSKIPSTR>[^\n@]+	;
<xSKIPSTR>\n		line_number++;
<xSKIPSTR>@@		;
<xSKIPSTR>@		BEGIN(xSKIP);

{num}			yylval.str = xstrdup(yytext); return tREV;
[:;$]			return *yytext;

<xID>{ws}+	;
<xID>\n		line_number++;
<xID>{special}	BEGIN(INITIAL); return *yytext;
<xID>{num}?{idchar}({idchar}|{num})*		{
		yylval.str = xstrdup(yytext);
		BEGIN(INITIAL);
		return tID;
	}
<xID>{num}?.	rcserror("Invalid character in ID '%s' (0x%02x)", yytext, yytext[yyleng-1]);

<xSYM>{ws}+	;
<xSYM>\n	line_number++;
<xSYM>{special}	BEGIN(INITIAL); return *yytext;
<xSYM>{digit}*{idchar}({idchar}|{digit})*	{
		yylval.str = xstrdup(yytext);
		BEGIN(INITIAL);
		return tSYM;
	}
<xSYM>{digit}*.	rcserror("Invalid character in SYMBOL '%s' (0x%02x)", yytext, yytext[yyleng-1]);

@		reset_str(); BEGIN(xSTR);
<xSTR>[^@\n]+	add_str(yytext, yyleng);
<xSTR>\n	line_number++; add_str(yytext, yyleng);
<xSTR>@@	add_str(yytext, 1);
<xSTR>@		yylval.str = get_str(); BEGIN(INITIAL); skip_string = 0; return tSTRING;

<xAUTHOR>@	reset_str(); BEGIN(xSTR);
<xAUTHOR>{ws}+	;
<xAUTHOR>\n	line_number++;
<xAUTHOR>{special}	BEGIN(INITIAL); return *yytext;
<xAUTHOR>({idchar}|{num}|{ws})+		{
		yylval.str = xstrdup(yytext);
		BEGIN(INITIAL);
		return tID;
	}
<xAUTHOR>.	rcserror("Invalid character in AUTHOR '%s' (0x%02x)", yytext, yytext[yyleng-1]);
	
{ws}+		; /* Ignore whitespace */
\n		line_number++;

.		rcserror("Unknown char/unmatched text '%c' (0x%02x)", isprint((int)((unsigned char)*yytext)) ? *yytext : ' ', *yytext);

%%

int yywrap(void)
{
	return 1;
}

void set_id(void)
{
	BEGIN(xID);
}

void set_author(void)
{
	BEGIN(xAUTHOR);
}

void set_sym(void)
{
	BEGIN(xSYM);
}

void set_skip(void)
{
	BEGIN(xSKIP);
}

void set_skipstr(void)
{
	skip_string = 1;
}

#define STRALLOCSIZE	256
static char *str;
static int nstr;
static int nastr;

static void reset_str(void)
{
	nstr = 0;
}

static void add_str(const char *s, int l)
{
#ifdef SKIP_DELTATEXT
	if(skip_string)
		return;
#endif
	if(nstr + l + 1 > nastr)
	{
		str = xrealloc(str, nastr+STRALLOCSIZE);
		nastr += STRALLOCSIZE;
	}
	memcpy(str+nstr, s, l);
	nstr += l;
}

static char *get_str(void)
{
#ifdef SKIP_DELTATEXT
	if(skip_string)
		return xstrdup("");
#endif
	if(str)
	{
		str[nstr] = '\0';
		return xstrdup(str);
	}
	else
		return xstrdup("");
}

